package CardPackage;

import java.util.ArrayList;
import java.util.Scanner;

public class Customer {

    static int customerCounter = 0;
    static ArrayList<Customer> customerList = new ArrayList<>();

    Scanner input = new Scanner(System.in);
    private int customerNumber;
    private String name;
    private String surname;
    private int age;

    /**
     * Constructor
     */

    public Customer() {
    }

    public Customer(int customerNumber, String name, String surname, int age) {
        this.customerNumber = customerNumber;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public void customerReg() {                                        /* ISLEYIR */
        Customer customer = new Customer();

        MenuCommands menuCommands = new MenuCommands();



        System.out.println("Please add information below for customer registration:");
        customer.setCustomerNumber((int) (Math.random() * (99999 - 10000 + 1) + 10000));
        System.out.println("Name:");
        customer.setName(input.next());
        System.out.println("Surname:");
        customer.setSurname(input.next());
        System.out.println("Age:");
        customer.setAge(input.nextInt());
        System.out.println("New customer created:");

        customerList.add(customerCounter, customer);
        menuCommands.appStart();
    }

    public void showCustomerList(){

        MenuCommands menuCommands = new MenuCommands();

        if (customerList.size()==0){
            System.out.println("The List Is Emphty");
            menuCommands.appStart();
        } else {
            for(Customer i: customerList){
                System.out.println(i);
            }
            menuCommands.appStart();
        }

    }


    //****************************************
    @Override
    public String toString() {
        return "customerNumber=" + customerNumber + "/ name=" + name + "/ surname=" + surname + "/ age=" + age;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }
}