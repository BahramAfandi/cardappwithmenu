package CardPackage;

import java.util.Scanner;

import static CardPackage.Customer.customerList;
import static CardPackage.Card.cardList;

public class MenuCommands {
    Scanner input = new Scanner(System.in);

    public void appStart() {                                        /* ISLEYIR */
        Customer customer = new Customer();
        Card cardReg = new Card();

        System.out.println("** Please select operation **");
        System.out.println("-->0. Import from DataBase");
        System.out.println("-->1. Customer Registration");
        System.out.println("-->2. Card Registration");
        System.out.println("-->3. Show Customer List");
        System.out.println("-->4. Card Operations");
        System.out.println("-->5. Exit");

        int operation = input.nextInt();


        if (operation == 0) {
            DataBase dataBase = new DataBase();
            customerList= dataBase.customerData();
            appStart();
        } else if (operation == 1) {
            customer.customerReg();
        } else if (operation == 2) {
            if (customerList.size() == 0) {
                customer.customerReg();
            } else {
                cardReg.selectCustomer();
            }
        } else if (operation == 3) {
            customer.showCustomerList();
        } else if (operation == 4) {
            if (customerList.size() == 0 || cardList.size() == 0) {
                System.out.println("**Please firstly registrate your customer and card**");
                appStart();
            } else {
                menuCardOperations();
            }
        } else if (operation == 5) {
            System.exit(0);
        } else {
            appStart();
        }
    }

    public void nextAfterCardReg() {
        System.out.println("** Select Your Next Operation **");
        String[] nextAfterCardReg = new String[]{"0-->Back to RegScreen", "1-->Card Operations", "3-->Exit program"};

        for (String i : nextAfterCardReg) {
            System.out.println(i);
        }

        Scanner input = new Scanner(System.in);
        int selected = input.nextInt();
        if (selected == 0) {
            appStart();
        } else if (selected == 1) {
            menuCardOperations();
        } else if (selected == 2) {
            System.exit(0);
        } else {
            nextAfterCardReg();
        }
    }

    public void menuCardOperations() {
        System.out.println("Card Operations");
        String[] cardOperationList = new String[9];
        cardOperationList[0] = "-->0. Back To Main Menu";
        cardOperationList[1] = "-->1. Get full customerList";
        cardOperationList[2] = "-->2. Get full cardList";
        cardOperationList[3] = "-->3. Get ActiveCards";
        cardOperationList[4] = "-->4. Get InActiveCards";
        cardOperationList[5] = "-->5. Get ActiveCards by Customer Number";
        cardOperationList[6] = "-->6. Get InActiveCards by Customer Number";
        cardOperationList[7] = "-->7. ---";
        cardOperationList[8] = "-->8. Exit";

        for (String operations : cardOperationList) {
            System.out.println(operations);
        }

        Scanner input = new Scanner(System.in);
        CardApplication cardApplication = new CardApplication();

        int selectedOperationIndex = input.nextInt();
        switch (selectedOperationIndex) {
            case 0:
                appStart();
                break;
            case 1:
                cardApplication.forEachCustomerList();
                break;
            case 2:
                cardApplication.forEachCardList();
                break;
            case 3:
                cardApplication.getActiveCards();
                break;
            case 4:
                cardApplication.getInActiveCards();
                break;
            case 5:
                cardApplication.getActiveCardsByCustomerNumber();
                break;
            case 6:
                cardApplication.getInActiveCardsByCustomerNumber();
                break;
            case 7:
                System.out.println("Emphty Method");
                break;
        }
        menuCardOperations();
    }
}
