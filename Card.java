package CardPackage;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

import static CardPackage.Customer.customerList;

public class Card {

    Scanner input = new Scanner(System.in);
    static Customer selectedCustomer;
    static ArrayList<Card> cardList = new ArrayList<>();

    private long cardNumber; // 16 digit unique
    private String cardType;
    private String cardProductType;
    private double availableBalance;
    private String cardHolderName;
    private String currency;
    private String status;
    private LocalDate openDate;
    private LocalDate expireDate;
    private int customerNumber; // length 6
    private String productCode; // unique, max length 6
    private String bankName;


    public Card() {
    }

    public Card(long cardNumber, String cardHolderName, String bankName, String cardType, String cardProductType,
                String currency, Double availableBalance,  String status,
                LocalDate openDate, LocalDate expireDate, int customerNumber,
                String productCode) {
        this.cardNumber = cardNumber;
        this.cardType = cardType;
        this.cardProductType = cardProductType;
        this.availableBalance = availableBalance;
        this.currency = currency;
        this.status = status;
        this.openDate = openDate;
        this.expireDate = expireDate;
        this.customerNumber = customerNumber;
        this.productCode = productCode;
        this.bankName = bankName;
    }

    public void selectCustomer() {

        System.out.println("***Select Customer for Card Registration***");

        int counter = 0;
        for (Customer i : customerList) {
            System.out.println((counter++) + "-->" + i);
        }

        int select = input.nextInt();
        selectedCustomer = customerList.get(select);
        cardRegistration();
    }

    public void cardRegistration() {

        MenuCommands menuCommands = new MenuCommands();
        CardSettings cardSettings = new CardSettings();
        Customer customer = new Customer();

        System.out.println("Selected customer is: " + selectedCustomer);

        Card card = new Card(
                cardSettings.randomCardNumber(), (customer.getName()+" "+customer.getSurname()), cardSettings.selectBank(), cardSettings.selectCardType(), cardSettings.selectCardProductType(),
                cardSettings.selectCurrency(), cardSettings.inputBalance(), cardSettings.selectStatus(),
                cardSettings.openDate(), expireDate, customer.getCustomerNumber(),
                cardSettings.selectProductCode());

        cardList.add(card);

        System.out.println(cardList);

        menuCommands.appStart();

        /** TAMAMLANMAYIB!!! */
        /** TAMAMLANMAYIB!!! */
        /** TAMAMLANMAYIB!!! */
        /** TAMAMLANMAYIB!!! */
        /** TAMAMLANMAYIB!!! */
        /** TAMAMLANMAYIB!!! */
    }


/*    public void getCustomerCards(){
        Iterator<Card> iterate = CardApplication.cardList.iterator();
        while (iterate.hasNext()){
            System.out.println(iterate.next());
        }
    }*/


    /**
     * SETTER / GETTER
     */

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return cardType.equals(card.cardType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardType);
    }



    @Override
    public String toString() {
        return "Card: " +
                "cardNumber='" + cardNumber + '\'' +
                ", cardType='" + cardType + '\'' +
                ", cardProductType='" + cardProductType + '\'' +
                ", availableBalance=" + availableBalance +
                ", currency='" + currency + '\'' +
                ", status='" + status + '\'' +
                ", openDate=" + openDate +
                ", expireDate=" + expireDate +
                ", customerNumber='" + customerNumber + '\'' +
                ", productCode='" + productCode + '\'' +
                ", bankName='" + bankName + '\'';
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setCardProductType(String cardProductType) {
        this.cardProductType = cardProductType;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setOpenDate(LocalDate openDate) {
        this.openDate = openDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardProductType() {
        return cardProductType;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public String getCurrency() {
        return currency;
    }

    public String getStatus() {
        return status;
    }

    public LocalDate getOpenDate() {
        return openDate;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getBankName() {
        return bankName;
    }
}
