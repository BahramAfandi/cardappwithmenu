package CardPackage;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class CardSettings {

    String[] bankList = new String[]{"ABB", "UNIBANK", "KAPITAL", "LEOBANK"};
    String[] cardProductTypeList = new String[]{"STANDART", "MASTERCARD", "VISA"};
    String[] status = new String[]{"ACTIVE", "INACTIVE"};
    String[] cardTypeList = new String[]{"DEBIT", "KREDIT", "SALARY", "PENSION", "MILES", "AMEX"};
    String[] currencyList = new String[]{"AZN", "USD", "EURO"};
    String[] productCode = new String[]{"010MML", "001AMX"};
    static LocalDate openDate;
    static LocalDate expireDate;

    public LocalDate openDate() {
        int randomDays = (int) (Math.random() * (300 - 100) + 100);
        openDate = LocalDate.now().minusDays(randomDays);
        int randomYears = (int) (Math.random() * (3 - 1) + 1);
        expireDate=openDate.plusYears(randomYears);
        return openDate;
    }

    public LocalDate expireDate() {
        return expireDate;
    }


    public String selectProductCode() {
        int randomSelect = new Random().nextInt(productCode.length);
        return productCode[randomSelect];
    }

    public String selectCurrency() {
        Scanner input = new Scanner(System.in);
        int counter = 0;
        System.out.println("**Please select your CURRENCY**");
        for (String i : currencyList) {
            System.out.println("->>" + counter++ + ". " + i);
        }
        int selectedCurrency = input.nextInt();
        return (currencyList[selectedCurrency]);
    }

    public String selectCardType() {
        Scanner input = new Scanner(System.in);
        int counter = 0;
        System.out.println("**Please select your CARD TYPE**");
        for (String i : cardTypeList) {
            System.out.println("->>" + counter++ + ". " + i);
        }

        int selectedCardType = input.nextInt();
        return (cardTypeList[selectedCardType]);
    }

    public String selectStatus() {
        int randomSelect = new Random().nextInt(status.length);
        return status[randomSelect];
    }

    public double inputBalance() {
        System.out.println("*** INPUT_YOUR_CASH ***");
        Scanner input = new Scanner(System.in);
        return input.nextDouble();
    }

    public String selectBank() {

        int counter = 0;
        System.out.println("**Please Select Your Bank");

        for (String i : bankList) {
            System.out.println("->>" + counter++ + ". " + i);
        }

        Scanner input = new Scanner(System.in);
        int selection = input.nextInt();
        return bankList[selection];
    }

    public long randomCardNumber() {
        long cardNumber;
        cardNumber = ((long) (Math.random() * (9999999999999999L - 1000000000000000L + 1) + 100000000000L));
        return cardNumber;
    }


    public String selectCardProductType() {
        Scanner input = new Scanner(System.in);
        int counter = 0;
        System.out.println("**Please select your CARD PRODUCT TYPE**");
        for (String i : cardProductTypeList) {
            System.out.println("->>" + counter++ + ". " + i);
        }
        int selectedProductType = input.nextInt();
        return (cardProductTypeList[selectedProductType]);
    }
}
