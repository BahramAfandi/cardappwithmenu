package CardPackage;

import java.util.ArrayList;
import java.util.Random;

import static CardPackage.Card.selectedCustomer;
import static CardPackage.Customer.customerList;

public class DataBase {
    public ArrayList<Customer> customerData() {
        ArrayList<Customer> customerData = new ArrayList<>();

        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Bahram", "Afandi", 27));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Orxan", "Nasirov", 29));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Nicat", "Axundov", 27));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Eldar", "Namazov", 62));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Vusal", "Orucov", 29));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Faqan", "Camalov", 34));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Murad", "Mustafayev", 25));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Sanan", "Suleymanov", 42));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Namiq", "Afandiyev", 57));
        customerData.add(new Customer(((int) (Math.random() * (99999 - 10000 + 1) + 10000)), "Elnur", "Ismayilov", 31));

        return customerData;
    }

/*    public ArrayList<Card> DataBase

    {
        CardSettings cardSettings = new CardSettings();
        ArrayList<Card> cardData = new ArrayList<>();

        for (int i = 0; i < customerList.size(); i++) {
            selectedCustomer = customerList.get(i);

            cardData.add(cardSettings.randomCardNumber(),(selectedCustomer.getName() + " " + selectedCustomer.getSurname()), )

        }

        cardData.add(4169236545325984, "BahramAfandi", 13152, "KAPITAL", "KREDIT", "VISA", "AZN", 2400.56, "ACTIVE");
        cardData.add(new Card());
        cardData.add(new Card());
        cardData.add(new Card());
        cardData.add(new Card());
        cardData.add(new Card());
        cardData.add(new Card());
        cardData.add(new Card());
        cardData.add(new Card());
        cardData.add(new Card());

        return cardData;
    }*/
}
