package CardPackage;

import java.util.Scanner;

import static CardPackage.Card.cardList;
import static CardPackage.Customer.customerList;


public class CardApplication {

    Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        MenuCommands menuCommands = new MenuCommands();
        menuCommands.appStart();
    }

    public void forEachCustomerList() {
        int counter = 1;

        for (Customer i : customerList) {
            System.out.println(counter++ + ". " + " " + i);
        }
    }

    public void forEachCardList() {
        int counter = 1;

        for (Card i : cardList) {
            System.out.println(counter++ + ". " + " " + i);
        }
    }

    public void getActiveCards() {
        cardList.
                stream()
                .filter(a->a.getStatus() != null)
                .filter(a->a.getStatus().equals("ACTIVE"))
                .forEach(System.out::println);
    }

    public void getInActiveCards() {
        cardList.
                stream()
                .filter(a->a.getStatus() != null)
                .filter(a->a.getStatus().equals("INACTIVE"))
                .forEach(System.out::println);
    }

    public void getActiveCardsByCustomerNumber() {
        System.out.println("**Please input CUSTOMER NUMBER for filtering**");
        int customerNumberInput = input.nextInt();

        cardList.
                stream()
                .filter(a->a.getStatus() != null)
                .filter(a->a.getCustomerNumber()==customerNumberInput)
                .filter(a->a.getStatus().equals("ACTIVE"))
                .forEach(System.out::println);
    }

    public void getInActiveCardsByCustomerNumber() {
        System.out.println("**Please input CUSTOMER NUMBER for filtering**");
        int customerNumberInput = input.nextInt();

        cardList.
                stream()
                .filter(a->a.getStatus() != null)
                .filter(a->a.getCustomerNumber()==customerNumberInput)
                .filter(a->a.getStatus().equals("InACTIVE"))
                .forEach(System.out::println);
    }
}